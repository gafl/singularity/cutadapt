# cutadapt Singularity container
### Bionformatics package cutadapt<br>
Trim adapters from high-throughput sequencing reads<br>
cutadapt Version: 2.10<br>
[https://cutadapt.readthedocs.io/]

Singularity container based on the recipe: Singularity.cutadapt_v2.10

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build cutadapt_v2.10.sif Singularity.cutadapt_v2.10`

### Get image help
`singularity run-help ./cutadapt_v2.10.sif`

#### Default runscript: STAR
#### Usage:
  `cutadapt_v2.10.sif --help`<br>
    or:<br>
  `singularity exec cutadapt_v2.10.sif cutadapt --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull cutadapt_v2.10.sif oras://registry.forgemia.inra.fr/gafl/singularity/cutadapt/cutadapt:latest`


